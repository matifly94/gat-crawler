import json

import fire

from gat.commands import get_sitemap, refresh_sitemap
import networkx as nx
import matplotlib.pyplot as plt

from gat.stats import get_summary


class SitemapInterface(object):
    def export_graph_json(self):
        return nx.node_link_data(get_sitemap().graph)

    def show_summary(self):
        return json.dumps(get_summary(sitemap=get_sitemap()))

    def refresh_sitemap(self):
        refresh_sitemap()

    def shortest_path(self, source, target):
        path = get_sitemap().shortest_path(source=source, target=target)
        return json.dumps({"distance": len(path), "path": path})

    def draw(self):
        nx.draw(get_sitemap().internal_graph, with_labels=True)
        plt.show()


if __name__ == "__main__":
    fire.Fire(SitemapInterface)

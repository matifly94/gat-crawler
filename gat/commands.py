import os
import pickle

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from settings import settings
from gat.models import SiteMap


def refresh_sitemap():
    run_crawler()
    if os.path.exists(settings.SITEMAP_FILE_NAME):
        os.remove(settings.SITEMAP_FILE_NAME)

    sitemap = SiteMap(pages_info=load_crawler_result())
    with open(settings.SITEMAP_FILE_NAME, "wb") as file:
        pickle.dump(sitemap, file=file)
    return sitemap


def load_crawler_result():
    return pickle_load(settings.CRAWLER_RESULT_FILE_NAME)


def load_sitemap():
    return pickle_load(settings.SITEMAP_FILE_NAME)


def get_sitemap():
    if not os.path.isfile("sitemap"):
        return refresh_sitemap()
    return load_sitemap()


def run_crawler():
    process = CrawlerProcess(get_project_settings())
    process.crawl("gat")
    process.start()


def pickle_load(file_name):
    with open(file_name, "rb") as file:
        data = pickle.load(file)
    return data

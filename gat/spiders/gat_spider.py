import pickle
from typing import Dict

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule

from settings import settings
from gat.models import PageInfo


def gat_url_filter(url):
    return url.startswith("http") or url.startswith("/")


def get_full_url(path: str) -> str:
    if not path.startswith("/"):
        return path
    return f"{settings.HOME_PAGE.rstrip('/')}{path}"


class GatSpider(scrapy.Spider):
    name = "gat"
    _pages_info: Dict[str, PageInfo] = {}
    allowed_domains = ["globalapptesting.com"]
    rule = Rule(LinkExtractor(), follow=True)

    def start_requests(self):
        urls = ["https://www.globalapptesting.com/"]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response, **kwargs):
        page_info = PageInfo(url=response.url, status=response.status, size=len(response.body))
        self._pages_info[response.url] = page_info
        if page_info.status >= 400:
            return
        links = self.rule.link_extractor.extract_links(response)
        urls = [link.url for link in self.rule.process_links(links)]
        page_info.links = urls
        for url in urls:
            if url in self._pages_info:
                continue
            yield response.follow(url=url, callback=self.parse)

    def close(self, reason):
        with open(settings.CRAWLER_RESULT_FILE_NAME, "wb") as file:
            pickle.dump(list(self._pages_info.values()), file=file)

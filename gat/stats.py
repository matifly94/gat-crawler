from gat.models import SiteMap


def get_summary(sitemap: SiteMap):
    return {
        "total_websites": len(sitemap.pages_info),
        "avg_external_links": sitemap.avg_external_links,
        "avg_internal_links": sitemap.avg_internal_links,
        "dead_urls": sitemap.dead_urls,
        "avg_size [bytes]": sitemap.avg_size,
        "most_linked": {k: v for k, v in sitemap.most_linked(qty=10)},
        "longest_path": sitemap.longest_path,
    }

from gat.models import PageInfo, SiteMap


class TestPageInfo:
    def test_links(self):
        page_info = PageInfo(
            url="https://www.example.com/",
            status=200,
            size=0,
            links=[
                "https://www.example.com/how-we-help/cio-cto",
                "https://www.example.com/",
                "https://www.linkedin.com/company/global-app-testing",
                "https://www.google.com/search?q=https://www.example.com/",
            ],
        )
        assert page_info.external_links == [
            "https://www.linkedin.com/company/global-app-testing",
            "https://www.google.com/search?q=https://www.example.com/",
        ]
        assert page_info.internal_links == [
            "https://www.example.com/how-we-help/cio-cto,",
            "https://www.example.com/",
        ]


class TestSiteMap:
    def setup(self):
        self.sitemap = SiteMap(
            [
                PageInfo(url="1", status=200, size=0, links=["2", "3"]),
                PageInfo(url="2", status=200, size=0, links=["4"]),
                PageInfo(url="3", status=200, size=0, links=["5"]),
                PageInfo(url="4", status=200, size=0, links=["5"]),
                PageInfo(url="5", status=400, size=0, links=["1"]),
            ]
        )

    def test_longest_path(self):
        assert self.sitemap.longest_path == ("2", "3", 4, ["2", "4", "5", "1", "3"])

    def test_shortest_path(self):
        assert self.sitemap.shortest_path("1", "5") == ["1", "3", "5"]

    def test_avg_external_links(self):
        assert self.sitemap.avg_external_links == 1.2

    def test_dead_urls(self):
        assert self.sitemap.dead_urls == [self.sitemap.pages_info[4]]

    def test_most_linked(self):
        assert self.sitemap.most_linked(qty=1) == [("5", 2)]

from collections import OrderedDict, defaultdict
from dataclasses import dataclass
from functools import cached_property
from statistics import mean
from typing import List

import networkx as nx

from settings import settings


@dataclass
class PageInfo:
    url: str
    status: int
    size: int = 0
    links: List = None

    @property
    def is_invalid(self):
        return self.status >= 400

    @cached_property
    def external_links(self):
        return list(filter(lambda link_url: not link_url.startswith(settings.HOME_PAGE), self.links))

    @cached_property
    def internal_links(self):
        return list(filter(lambda link_url: link_url.startswith(settings.HOME_PAGE), self.links))


class SiteMap:
    _links_count = None
    _graph = None

    def __init__(self, pages_info: List[PageInfo]):
        self.pages_info = pages_info
        self._build_graph()

    @property
    def graph(self):
        if self._graph is None:
            self._build_graph()
        return self._graph

    @property
    def internal_graph(self):
        graph = nx.DiGraph()
        for info in self.pages_info:
            for link in info.internal_links:
                graph.add_edge(info.url, link)
        return graph

    def _build_graph(self):
        graph = nx.DiGraph()
        for info in self.pages_info:
            for link in info.links:
                graph.add_edge(info.url, link)
        self._graph = graph

    def shortest_path(self, source, target):
        return nx.shortest_path(self.graph, source=source, target=target)

    @cached_property
    def longest_path(self):
        source, target, total_max = None, None, 0
        for url, links in nx.all_pairs_shortest_path_length(self.graph):
            costs = list(links.values())
            link_urls = list(links.keys())
            max_value = max(costs)
            if max_value > total_max:
                source, target, total_max = (
                    url,
                    link_urls[costs.index(max_value)],
                    max_value,
                )
        full_path = nx.shortest_path(self.graph, source, target)
        return source, target, total_max, full_path

    @cached_property
    def avg_external_links(self):
        return mean(map(lambda info: len(info.external_links), self.pages_info))

    @cached_property
    def avg_internal_links(self):
        return mean(map(lambda info: len(info.internal_links), self.pages_info))

    @cached_property
    def avg_size(self):
        return mean(map(lambda info: info.size, self.pages_info))

    @cached_property
    def dead_urls(self):
        return list(filter(lambda page: page.is_invalid, self.pages_info))

    def most_linked(self, qty=10):
        if self._links_count is None:
            self._links_count = self._count_pages_links()
        return list(self._links_count.items())[:qty]

    def _count_pages_links(self):
        links_count = defaultdict(int)
        for page in self.pages_info:
            for link in page.links:
                links_count[link] += 1

        links_count = OrderedDict(sorted(links_count.items(), key=lambda kv: kv[1], reverse=True))
        return links_count



## GAT sitemap

### Environment configuration 
```shell
pip install requirements -r requiremets.txt
```

### Commands

1. `show_summary`

Returns sitemap basic metrics json. If there is no sitemap file, calls `refresh_sitemap`underneath.

```shell
python runner.py show_summary
```

2. `refresh_sitemap`

Generates new sitemap

```shell
python runner.py refresh_sitemap
```


3`shortest_path`

Returns shortest path between two provided subpages. Requires full url.

```shell
python runner.py shortest_path {source} {target}
```



### Running tests

```shell
pytest
```
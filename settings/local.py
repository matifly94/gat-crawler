BOT_NAME = "gat"

SPIDER_MODULES = ["gat.spiders"]
NEWSPIDER_MODULE = "gat.spiders"

ROBOTSTXT_OBEY = True
HOME_PAGE = "https://www.globalapptesting.com/"
CRAWLER_RESULT_FILE_NAME = "crawler_result"
SITEMAP_FILE_NAME = "sitemap"
SITEMAP_GRAPH_FILE_NAME = "sitemap_graph"

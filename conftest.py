import pytest
from simple_settings.strategies import SettingsLoadStrategyPython

from settings import settings


@pytest.fixture(autouse=True)
def change_settings():
    test = SettingsLoadStrategyPython.load_settings_file("settings.test")
    settings.configure(**test)
